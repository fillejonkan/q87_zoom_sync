#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <syslog.h>
#include <math.h>
#include "ptz.h"
#include <glib/gprintf.h>

/******************** MACRO DEFINITION SECTION ********************************/

/**
 * Log message macro
 */
#define LOG(fmt, args...)   { syslog(LOG_INFO, fmt, ## args); \
    g_message(fmt, ## args); }

/**
 * Error message macro
 */
#define ERR(fmt, args...)   { syslog(LOG_ERR, fmt, ## args); \
    g_message(fmt, ## args); }

/**
 * Enable / disable verbose logging.
 */
#define VERBOSE(x)
//#define VERBOSE(x) x

/**
 * Base string for querying a PTZ for it's zoom position.
 */
#define QUERY_POSITION_BASE  "curl --anyauth http://127.0.0.1/axis-cgi/com/ptz.\
cgi?query=position\\&camera=%d -u %s:%s > /tmp/position.txt 2> /dev/null"

/**
 * Base string for setting thermal zoom position
 */
#define THERMAL_SET_POS_BASE "curl --anyauth http://127.0.0.1/axis-cgi/com/ptz.\
cgi?zoom=%d\\&camera=2 -u %s:%s"

/**
 * Camera ID used in VAPIX for visual channel.
 */
#define VISUAL_PTZ_CHANNEL  (1)

/**
 * Camera ID used in VAPIX for thermal channel.
 */
#define THERMAL_PTZ_CHANNEL (2)

/******************** LOCAL VARIABLE DECLARATION SECTION **********************/

/**
 * Current visual zoom position.
 */
static int cur_vis_pos = 1;

/**
 * Current thermal zoom position.
 */
static int cur_therm_pos = 1;

/**
 * Current visual zoom start (corresponding to thermal wide).
 */
static int  zoom_start = 877;

/**
 * Current visual zoom end (corresponding to thermal tele).
 */
static int  zoom_end   = 3861;

/**
 * Current PTZ username.
 */
static char *username  = NULL;

/**
 * Current PTZ password.
 */
static char *password  = NULL;

/******************** LOCAL FUNCTION DECLARATION SECTION **********************/

/**
 * Retrieve content of temporary file used for VAPIX commands.
 */
static char *get_file_buffer();

/**
 * Get zoom position for given video channel.
 */
static int get_zoom_position(int video_channel);

/**
 * Set thermal zoom to a given value.
 */
static int set_thermal_zoom(int position);

/******************** LOCAL FUNCTION DEFINITION SECTION ***********************/

/**
 * Retrieve content of temporary file used for VAPIX commands.
 */
static char *get_file_buffer()
{
    char *buffer = 0;
    long length;
    FILE *f = fopen ("/tmp/position.txt", "rb");

    if (f) {
        fseek (f, 0, SEEK_END);
        length = ftell (f);
        fseek (f, 0, SEEK_SET);
        buffer = malloc (length);
        if (buffer) {
            fread (buffer, 1, length, f);
        }
    }

    fclose(f);

    return buffer;
}

/**
 * Get zoom position for given video channel.
 */
static int get_zoom_position(int video_channel)
{
    char *cmd = g_strdup_printf(QUERY_POSITION_BASE, video_channel, username,
        password);

    system(cmd);

    g_free(cmd);

    char *buffer = get_file_buffer();

    if (!buffer) {
        return -1;
    }

    float pan = 0;
    float tilt = 0;
    int zoom = -1;
    int focus = 0;
    int brightness = 0;
    int iris = 0;
    char autofocus[3] = "on";
    char autoiris[3]  = "off";
    int ret = -1;

    if (video_channel == 1) {
        ret = sscanf(buffer,
            "pan=%f\ntilt=%f\nzoom=%d\nfocus=%d\nbrightness=%d\nautofocus="
            "%s\nautoiris=%s",
            &pan, &tilt, &zoom, &focus, &brightness, autofocus, autoiris);

        if (ret != 7) {
            ERR("Failed to parse PTZ position");
            goto cleanup;
        }

    } else {
        ret = sscanf(buffer, "pan=%f\ntilt=%f\nzoom=%d\niris=%d\nfocus=%d",
            &pan, &tilt, &zoom, &iris, &focus);

        if (ret != 5) {
            ERR("Failed to parse PTZ position");
            goto cleanup;
        }
    }

    VERBOSE(LOG("Got %d matches: pan=%f, tilt=%f, zoom=%d, focus=%d, "
        "autofocus=%s, autoiris=%s",
        ret, pan, tilt, zoom, focus, autofocus, autoiris));

cleanup:

    g_free(buffer);

    return zoom;
}

/**
 * Set thermal zoom to a given value.
 */
static int set_thermal_zoom(int position)
{
    position = CLAMP(position, 1, 9999);

    char *cmd = g_strdup_printf(THERMAL_SET_POS_BASE, position,
        username, password);

    VERBOSE(LOG("Setting thermal position %d, command: %s", position, cmd));

    usleep(10000);
    system(cmd);

    g_free(cmd);

    return 0;
}

/******************** EXTERNAL FUNCTION DEFINITION SECTION ********************/

/**
 * Initiate PTZ library.
 */
void ptz_init()
{
    cur_vis_pos   = get_zoom_position(VISUAL_PTZ_CHANNEL);
    cur_therm_pos = get_zoom_position(THERMAL_PTZ_CHANNEL);

    LOG("Initializing PTZ library. Visual Zoom: %d, Thermal Zoom: %d",
        cur_vis_pos, cur_therm_pos);
}

/**
 * Set zoom endpoints on the visual scale.
 */
void ptz_set_zoom_endpoints(int startz, int endz)
{
    zoom_start = startz;
    zoom_end   = endz;

    LOG("Got new PTZ endpoints %d, %d", zoom_start, zoom_end);
}

/**
 * Set PTZ username.
 */
void ptz_set_username(const char *user)
{
    g_free(username);

    username = g_strdup_printf("%s", user);

    LOG("Got new PTZ username %s", username);
}

/**
 * Set PTZ password.
 */
void ptz_set_password(const char *pass)
{
    g_free(password);

    password = g_strdup_printf("%s", pass);

    LOG("Got new PTZ password %s", password);
}

/**
 * Get PTZ username.
 */
const char *ptz_get_username()
{
    return username;
}

/**
 * Get PTZ password.
 */
const char *ptz_get_password()
{
    return password;
}

void ptz_zoom_sync()
{
    if (cur_vis_pos <= 0  || cur_therm_pos <= 0) {
        /* Try to get a correct position and then retry next timeout */
        cur_vis_pos   = get_zoom_position(VISUAL_PTZ_CHANNEL);
        cur_therm_pos = get_zoom_position(THERMAL_PTZ_CHANNEL);

        return;
    }

    int new_vis_pos   = get_zoom_position(VISUAL_PTZ_CHANNEL);
    int new_therm_pos = get_zoom_position(THERMAL_PTZ_CHANNEL);

    if (new_vis_pos <= 0 || new_therm_pos <= 0) {
        return;
    }

    if (new_vis_pos != cur_vis_pos) {
        VERBOSE(LOG("Got visual zoom position change %d --> %d", cur_vis_pos,
            new_vis_pos));

        /* Calculate new thermal position */
        int new_thermal_position = CLAMP(new_vis_pos, zoom_start, zoom_end);

        new_thermal_position = round(((float)new_thermal_position - zoom_start)
            / ((float) (zoom_end - zoom_start)) * 9999);

        new_thermal_position = CLAMP(new_thermal_position, 1, 9999);

        VERBOSE(LOG("Calculated new thermal position %d", new_thermal_position));

        cur_therm_pos = get_zoom_position(THERMAL_PTZ_CHANNEL);

        if (cur_therm_pos > 0 && cur_therm_pos != new_thermal_position) {
            /* Make sure that move is necessary */
            set_thermal_zoom(new_thermal_position);
        }

        cur_vis_pos = new_vis_pos;
    }
}
