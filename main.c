#include <stdlib.h>
#include <glib.h>
#include <glib/gprintf.h>

#include <stdio.h>
#include <syslog.h>
#include <signal.h>
#include <string.h>

#include "camera/camera.h"
#include "ptz.h"
/******************** MACRO DEFINITION SECTION ********************************/

/**
 * Log message macro
 */
#define LOG(fmt, args...)   { syslog(LOG_INFO, fmt, ## args); \
    g_message(fmt, ## args); }

/**
 * Error message macro
 */
#define ERR(fmt, args...)   { syslog(LOG_ERR, fmt, ## args); \
    g_message(fmt, ## args); }

/**
 * APP ID used for logs etc.
 */
#define APP_ID              "q87zoomsync"

/**
 * Nice name used for App
 */
#define APP_NICE_NAME       "AXIS Q87 Zoom Sync"

/**
 * Default value for PTZ zoom (VAPIX) corresponding to wide thermal FOV.
 */
#define DEFAULT_START_ZOOM_VAPIX (877)

/**
 * Default value for PTZ zoom (VAPIX) corresponding to tele thermal FOV.
 */
#define DEFAULT_END_ZOOM_VAPIX   (3861)

/**
 * Time between polling of zoom position for potential update.
 */
#define ZOOM_UPDATE_TIMEOUT_US (500000)

/******************** LOCAL VARIABLE DECLARATION SECTION **********************/


/**
 * Current value for PTZ zoom (VAPIX) corresponding to wide thermal FOV.
 */
static int start_zoom_vapix = DEFAULT_START_ZOOM_VAPIX;

/**
 * Current value for PTZ zoom (VAPIX) corresponding to tele thermal FOV.
 */
static int end_zoom_vapix   = DEFAULT_END_ZOOM_VAPIX;

/**
 * The GMainLoop context.
 */
static GMainLoop *loop;

/**
 * Condition used to exit polling thread
 */
static gboolean exit_thread;

/**
 * GLib condition variable used for signalling thread exit.
 */
static GCond exit_thread_c;

/**
 * Mutex needed when waiting in polling thread.
 */
static GMutex exit_thread_m;

/******************** LOCAL FUNCTION DECLARATION SECTION **********************/

/**
* Handle Unix signals.
*/
static void handle_sigterm(int signo);

/**
* Initiate signal handlers.
*/
static void init_signals();

/**
* CB for setting value for PTZ zoom (VAPIX) corresponding to wide thermal FOV.
*/
static void set_start_zoom(const char *value);

/**
* CB for setting value for PTZ zoom (VAPIX) corresponding to tele thermal FOV.
*/
static void set_end_zoom(const char *value);

/**
* CB for for setting PTZ username.
*/
static void set_username(const char *value);

/**
* CB for setting PTZ password.
*/
static void set_password(const char *value);

/**
* CB for retrieving parameters in Web Interface.
*/
static void api_settings_get(CAMERA_HTTP_Reply http,
                             CAMERA_HTTP_Options options);
/**
* CB for setting parameters through Web Interface.
*/
static void api_settings_set(CAMERA_HTTP_Reply http,
                             CAMERA_HTTP_Options options);

/**
* Poll for zoom changes that need compensation.
*/
static void *poll_thread(void *data);

/******************** LOCAL FUNCTION DEFINTION SECTION ************************/

/**
* Handle Unix signals.
*/
static void handle_sigterm(int signo)
{
    LOG("GOT SIGTERM OR SIGINT\n");
    g_main_loop_quit(loop);
}

/**
* Initiate signal handlers.
*/
static void init_signals()
{
    struct sigaction sa;
    sa.sa_flags = 0;

    sigemptyset(&sa.sa_mask);
    sa.sa_handler = handle_sigterm;
    sigaction(SIGTERM, &sa, NULL);
    sigaction(SIGINT, &sa, NULL);
}

/**
* CB for setting value for PTZ zoom (VAPIX) corresponding to wide thermal FOV.
*/
static void set_start_zoom(const char *value)
{
    start_zoom_vapix = CLAMP(atoi(value), 1, 9999);

    LOG("Got start zoom vapix: %d", start_zoom_vapix);

    ptz_set_zoom_endpoints(start_zoom_vapix, end_zoom_vapix);
}

/**
* CB for setting value for PTZ zoom (VAPIX) corresponding to tele thermal FOV.
*/
static void set_end_zoom(const char *value)
{
    end_zoom_vapix = CLAMP(atoi(value), 1, 9999);

    LOG("Got end zoom vapix: %d", end_zoom_vapix);

    ptz_set_zoom_endpoints(start_zoom_vapix, end_zoom_vapix);
}

/**
* CB for for setting PTZ username.
*/
static void set_username(const char *value)
{
    ptz_set_username(value);
}

/**
* CB for setting PTZ password.
*/
static void set_password(const char *value)
{
    ptz_set_password(value);
}

/**
* CB for retrieving parameters in Web Interface.
*/
static void api_settings_get(CAMERA_HTTP_Reply http,
                             CAMERA_HTTP_Options options)
{
    camera_http_sendXMLheader(http);
    camera_http_output(http, "<settings>");
    camera_http_output(http, "<param name='StartZoom' value='%d'/>",
        start_zoom_vapix);
    camera_http_output(http, "<param name='EndZoom' value='%d'/>",
        end_zoom_vapix);
    camera_http_output(http, "<param name='PTZUsername' value='%s'/>",
        ptz_get_username());
    camera_http_output(http, "<param name='PTZPassword' value='%s'/>",
        ptz_get_password());
    camera_http_output(http, "</settings>");
}

/**
* CB for setting parameters through Web Interface.
*/
static void api_settings_set(CAMERA_HTTP_Reply http,
                             CAMERA_HTTP_Options options)
{
    const char *value;
    const char *param;

    camera_http_sendXMLheader(http);

    param = camera_http_getOptionByName(options, "param");
    value = camera_http_getOptionByName(options, "value");

    if(!(param && value)) {
        camera_http_output(http,
            "<error description='Syntax: param or value missing'/>");
        ERR("api_settings_set: param or value is missing\n");
        return;
    }

    if(!camera_param_set(param, value)) {
        camera_http_output(http,
            "<error description='Could not set %s to %s'/>",  param, value);
            ERR("api_settings_set: Could not set %s to %s\n", param, value);
        return;
    }
    camera_http_output(http, "<success/>");
}

static void *poll_thread(void *data)
{
    gint64 start_time;
    gint64 end_time;

    (void) data;


    while (!exit_thread) {
        start_time = g_get_monotonic_time();
        end_time = start_time + ZOOM_UPDATE_TIMEOUT_US;

        g_mutex_lock(&exit_thread_m);
        while(g_cond_wait_until(&exit_thread_c, &exit_thread_m, end_time)) {
            if(exit_thread) {
                return NULL;
        }
        end_time = start_time + ZOOM_UPDATE_TIMEOUT_US;

        }
        g_mutex_unlock(&exit_thread_m);

        ptz_zoom_sync();
    }

    return NULL;
}

/**
* Main entry point for application.
*/
int main(int argc, char *argv[])
{
    openlog(APP_ID, LOG_PID | LOG_CONS, LOG_USER);
    camera_init(APP_ID, APP_NICE_NAME);
    init_signals();

    loop = g_main_loop_new(NULL, FALSE);

    char  value[50];
    if(camera_param_get("StartZoom", value, 50)) {
        set_start_zoom(value);
    }

    if(camera_param_get("EndZoom", value, 50)) {
        set_end_zoom(value);
    }

    if(camera_param_get("PTZUsername", value, 50)) {
        set_username(value);
    }

    if(camera_param_get("PTZPassword", value, 50)) {
        set_password(value);
    }

    ptz_init();

    camera_param_setCallback("StartZoom", set_start_zoom);
    camera_param_setCallback("EndZoom", set_end_zoom);
    camera_param_setCallback("PTZPassword", set_password);
    camera_param_setCallback("PTZUsername", set_username);

    camera_http_setCallback("settings/get", api_settings_get);
    camera_http_setCallback("settings/set", api_settings_set);

    /* Setup polling thread */
    g_cond_init(&exit_thread_c);
    g_mutex_init(&exit_thread_m);

    exit_thread = FALSE;
    GThread *pollt = g_thread_new("poll_thread", poll_thread, NULL);

    g_main_loop_run(loop);
    exit_thread = TRUE;

    LOG("EXITING APPLICATION\n");

    g_cond_signal(&exit_thread_c);
    g_thread_join(pollt);
    g_main_loop_unref(loop);

    camera_cleanup();
    closelog();

    return EXIT_SUCCESS;
}
