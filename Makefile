AXIS_USABLE_LIBS = UCLIBC GLIBC
include $(AXIS_TOP_DIR)/tools/build/rules/common.mak
include $(AXIS_TOP_DIR)/tools/build/rules/recurse.mak
include $(AXIS_TOP_DIR)/tools/build/rules/filepp.mak

PROG     = q87zoomsync

CFLAGS   +=
LDFLAGS  += -lm -laxparameter -lcapture -lstatuscache -ldl -lrt

PKGS = glib-2.0 gio-2.0 fixmath axptz axhttp axparameter axevent
CFLAGS += $(shell PKG_CONFIG_PATH=$(PKG_CONFIG_LIBDIR) pkg-config --cflags $(PKGS))
LDLIBS += $(shell PKG_CONFIG_PATH=$(PKG_CONFIG_LIBDIR) pkg-config --libs $(PKGS))

SRCS      = main.c ptz.c camera/camera.c
OBJS      = $(SRCS:.c=.o)

all: $(PROG) $(OBJS)

$(PROG): $(OBJS)
	$(CC) $(LDFLAGS) $^ $(LDLIBS) -o $@

clean:
	rm -f $(PROG) $(OBJS)
