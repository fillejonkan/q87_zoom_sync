#ifndef INCLUSION_GUARD_PTZ_H
#define INCLUSION_GUARD_PTZ_H

/******************** EXTERNAL FUNCTION DECLARATION SECTION *******************/

/**
 * Initiate PTZ library.
 */
void ptz_init();

/**
 * Set zoom endpoints on the visual scale.
 */
void ptz_set_zoom_endpoints(int startz, int endz);

/**
 * Set PTZ username.
 */
void ptz_set_username(const char *username);

/**
 * Set PTZ password.
 */
void ptz_set_password(const char *password);

/**
 * Get PTZ username
 */
const char *ptz_get_username();

/**
 * Get PTZ password.
 */
const char *ptz_get_password();

/**
 * Synchronize zoom values for thermal and visual camera.
 */
void ptz_zoom_sync();

#endif // PTZ_H